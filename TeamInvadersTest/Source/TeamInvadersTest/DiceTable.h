// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "InteractibleObjectInterface.h"
#include "Kismet/GameplayStatics.h"
#include "Components/InputComponent.h"
#include "Components/BoxComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Camera/CameraActor.h"
#include "DiceTable.generated.h"


UENUM()
enum class DiceTableStates : uint8
{
	Near,
	Far,
	Interactible
};

class ATeamInvadersTestCharacter;
class ATeamInvadersTestGameMode;
class ADice;

UCLASS()
class TEAMINVADERSTEST_API ADiceTable : public AActor, public IInteractibleObjectInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ADiceTable();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		ATeamInvadersTestCharacter* player;

	UPROPERTY(EditAnywhere)
		ATeamInvadersTestGameMode* gameMode;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* mesh;

	UPROPERTY(EditAnywhere)
		ACameraActor* tableCamera;

	UPROPERTY(EditAnywhere)
		DiceTableStates tableState = DiceTableStates::Far;

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(VisibleAnywhere)
		UBoxComponent* triggerCollider;

	UPROPERTY(EditAnywhere)
		ADice* dice1;
	UPROPERTY(EditAnywhere)
		ADice* dice2;
	UPROPERTY(EditAnywhere)
		ADice* dice3;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void OnInteraction() override;

	//Throw Dice
	UFUNCTION(BlueprintCallable)
		void ThrowDice(int amountOfDices);

private:

	UPROPERTY(EditDefaultsOnly)
		int distanceFromPlayer;

	UPROPERTY(EditAnywhere)
		UMaterial* nearMaterial;
	UPROPERTY(EditAnywhere)
		UMaterial* interactibleMaterial;

	UMaterial* defaultMaterial;

	void CheckDistance();

	void ChangeMaterial();

	void inline StartMinigame();
};
