// Fill out your copyright notice in the Description page of Project Settings.


#include "HUDManager.h"
#include "TeamInvadersTestGameMode.h"
#include "CustomDiceWidget.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "DiceTable.h"


void AHUDManager::ThrowDice(int amountOfDices)
{
	TArray<ADiceTable*> diceTables;
	TSubclassOf<ADiceTable> classType = ADiceTable::StaticClass();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), classType, diceTables);

	diceTable = diceTables[0];

	diceTable->ThrowDice(amountOfDices);

	//diceWidget->AddScore(amountOfDices);
}

void AHUDManager::ShowDiceHUD()
{
	auto GameMode = Cast<ATeamInvadersTestGameMode>(GetWorld()->GetAuthGameMode());

	widget = UWidgetBlueprintLibrary::Create(
		GetWorld(),
		GameMode->DiceWidgetBP,
		GetWorld()->GetFirstPlayerController()
	);

	diceWidget = Cast<UCustomDiceWidget>(widget);

	widget->AddToViewport();

	ShowHUD();
}
