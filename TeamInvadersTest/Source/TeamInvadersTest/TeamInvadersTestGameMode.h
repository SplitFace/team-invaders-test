// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Dice.h"
#include "TeamInvadersTestGameMode.generated.h"

UCLASS(minimalapi)
class ATeamInvadersTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATeamInvadersTestGameMode();

	//Widget BP
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> DiceWidgetBP;

	//Dice BP
	UPROPERTY(EditAnywhere)
		ADice* diceBP;
};



