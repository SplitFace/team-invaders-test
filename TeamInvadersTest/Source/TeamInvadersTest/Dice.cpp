// Fill out your copyright notice in the Description page of Project Settings.


#include "Dice.h"
#include "HUDManager.h"

// Sets default values
ADice::ADice()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ADice::BeginPlay()
{
	Super::BeginPlay();

	TArray<UStaticMeshComponent*> Components;
	GetComponents<UStaticMeshComponent>(Components, true);

	mesh = Components[0];
}

void ADice::throwThis()
{
	mesh->AddForce(FVector::UpVector * 100.0f);
}

// Called every frame
void ADice::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector currentVelocity = GetVelocity();

	if (currentVelocity.IsNearlyZero(0.1f))
	{
		if (FVector::DotProduct(FVector::UpVector, GetActorForwardVector()) == 1)
		{

		}
		else if (FVector::DotProduct(FVector::UpVector, GetActorRightVector()) == 1)
		{

		}
		else if (FVector::DotProduct(FVector::UpVector, GetActorUpVector()) == 1)
		{

		}
		else if (FVector::DotProduct(FVector::UpVector, -GetActorForwardVector()) == 1)
		{

		}
		else if (FVector::DotProduct(FVector::UpVector, -GetActorRightVector()) == 1)
		{

		}
		else if (FVector::DotProduct(FVector::UpVector, -GetActorUpVector()) == 1)
		{

		}
	}
}

