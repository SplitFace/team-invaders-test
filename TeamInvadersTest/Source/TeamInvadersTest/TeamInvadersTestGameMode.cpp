// Copyright Epic Games, Inc. All Rights Reserved.

#include "TeamInvadersTestGameMode.h"
#include "TeamInvadersTestCharacter.h"
#include "HUDManager.h"
#include "UObject/ConstructorHelpers.h"

ATeamInvadersTestGameMode::ATeamInvadersTestGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	HUDClass = AHUDManager::StaticClass();
}
