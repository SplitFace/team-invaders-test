// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CustomDiceWidget.generated.h"

/**
 *
 */
UCLASS()
class TEAMINVADERSTEST_API UCustomDiceWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintImplementableEvent, Category = "Dice")
		void AddScore(int score);
};
