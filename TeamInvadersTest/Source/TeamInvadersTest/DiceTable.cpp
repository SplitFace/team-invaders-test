// Fill out your copyright notice in the Description page of Project Settings.


#include "DiceTable.h"
#include "Dice.h"
#include "TeamInvadersTestCharacter.h"
#include "TeamInvadersTestGameMode.h"
#include "HUDManager.h"

// Sets default values
ADiceTable::ADiceTable()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Set up mesh
	if (!IsValid(RootComponent))
	{
		RootComponent = CreateDefaultSubobject<USceneComponent>("Root");
	}

	//Cube Mesh
	mesh = CreateDefaultSubobject<UStaticMeshComponent>("CubeMesh");
	static ConstructorHelpers::FObjectFinder<UStaticMesh> cube(TEXT("/Game/Geometry/Meshes/1M_Cube"));
	mesh->SetStaticMesh(cube.Object);
	mesh->SetCollisionProfileName(UCollisionProfile::BlockAll_ProfileName);
	mesh->SetupAttachment(RootComponent);
	mesh->SetSimulatePhysics(false);

	//Mesh material
	defaultMaterial = mesh->GetMaterial(0)->GetMaterial();

	//Collision
	triggerCollider = CreateDefaultSubobject<UBoxComponent>("Trigger Collider");
	triggerCollider->SetRelativeScale3D(FVector::OneVector + 2.0f);
	triggerCollider->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);
	triggerCollider->OnComponentBeginOverlap.AddDynamic(this, &ADiceTable::OnOverlapBegin);
	triggerCollider->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ADiceTable::BeginPlay()
{
	Super::BeginPlay();

	player = (ATeamInvadersTestCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	gameMode = (ATeamInvadersTestGameMode*)GetWorld()->GetAuthGameMode();
}

// Called every frame
void ADiceTable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CheckDistance();

	ChangeMaterial();
}

void ADiceTable::CheckDistance()
{
	distanceFromPlayer = (player->GetActorLocation() - GetActorLocation()).Size();

	if (distanceFromPlayer < 150)
	{
		tableState = DiceTableStates::Interactible;
	}
	else if (distanceFromPlayer < 700)
	{
		tableState = DiceTableStates::Near;
	}
	else
	{
		tableState = DiceTableStates::Far;
	}
}

void ADiceTable::ChangeMaterial()
{
	switch (tableState)
	{
	case DiceTableStates::Far:

		//GEngine->AddOnScreenDebugMessage(1, 1, FColor::Red, "Far Material");
		mesh->SetMaterial(0, defaultMaterial);

		break;


	case DiceTableStates::Near:

		//GEngine->AddOnScreenDebugMessage(1, 1, FColor::Red, "Near Material");
		mesh->SetMaterial(0, nearMaterial);

		break;

	case DiceTableStates::Interactible:

		//GEngine->AddOnScreenDebugMessage(1, 1, FColor::Red, "Interactible Material");
		mesh->SetMaterial(0, interactibleMaterial);

		break;

	default:
		break;
	}
}

void ADiceTable::ThrowDice(int amountOfDices)
{
	switch (amountOfDices)
	{
	case 3:

		dice1->throwThis();

	case 2:

		dice2->throwThis();

	case 1:

		dice3->throwThis();
		break;

	default:
		break;
	}
}

void ADiceTable::OnInteraction()
{
	if (tableState == DiceTableStates::Interactible)
	{
		StartMinigame();
		auto playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

		GEngine->AddOnScreenDebugMessage(1, 1, FColor::Red, "Interacted correctly");
	}
}

void ADiceTable::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Cast<ATeamInvadersTestCharacter>(OtherActor))
	{
		player->interactibleObject = this;
	}
}

void inline ADiceTable::StartMinigame()
{
	auto playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	//playerController->SetViewTarget(tableCamera);
	playerController->SetViewTargetWithBlend(tableCamera, 1);

	player->inputEnabled = false;

	playerController->bShowMouseCursor = true;
	playerController->bEnableClickEvents = true;
	playerController->bEnableMouseOverEvents = true;

	auto hud = Cast<AHUDManager>(playerController->GetHUD());

	hud->ShowDiceHUD();
}
