// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Blueprint/UserWidget.h"
#include "CustomDiceWidget.h"
#include "Kismet/GameplayStatics.h"
#include "HUDManager.generated.h"

/**
 *
 */

class ADiceTable;

UCLASS()
class TEAMINVADERSTEST_API AHUDManager : public AHUD
{
	GENERATED_BODY()
		//Throw Dice
		UFUNCTION(BlueprintCallable)
		void ThrowDice(int amountOfDices);

private:
	UUserWidget* widget;

	UCustomDiceWidget* diceWidget;

public:

	ADiceTable* diceTable;

public:
	//Show HUD Dice
	void ShowDiceHUD();

};
