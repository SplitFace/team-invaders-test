// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class TeamInvadersTestTarget : TargetRules
{
    public TeamInvadersTestTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Game;
        DefaultBuildSettings = BuildSettingsVersion.V2;
        ExtraModuleNames.Add("TeamInvadersTest");
        ExtraModuleNames.Add("Slate");
        ExtraModuleNames.Add("SlateCore");
        ExtraModuleNames.Add("UMG");
    }
}
